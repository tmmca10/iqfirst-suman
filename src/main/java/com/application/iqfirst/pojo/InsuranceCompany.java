package com.application.iqfirst.pojo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "InsuranceCompany")
public class InsuranceCompany implements Serializable{
	
	@Column(name = "CompId")
	@Id
	Long 	compId;
	
	@Column(name = "CompName")
	String 	compName;

	public InsuranceCompany(){}
	
	public InsuranceCompany(Long compId, String compName) {
		this.compId = compId;
		this.compName = compName;
	}

	public Long getCompId() {
		return compId;
	}

	public void setCompId(Long compId) {
		this.compId = compId;
	}

	public String getCompName() {
		return compName;
	}

	public void setCompName(String compName) {
		this.compName = compName;
	}

	@Override
	public String toString() {
		return "InsuranceCompany [compId=" + compId + ", compName=" + compName + "]";
	}

}
