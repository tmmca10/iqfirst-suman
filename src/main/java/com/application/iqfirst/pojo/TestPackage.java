package com.application.iqfirst.pojo;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "TestPackage")
public class TestPackage implements Serializable{
	
	@Column(name = "PackageId")
	@Id
	Long packageId;
	
	@Column(name = "PackageCd")
	String packageCd;
	
	@Column(name = "PackageTests")
	String packageTests;
	
	/*@ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                CascadeType.PERSIST,
                CascadeType.MERGE
            },
            mappedBy = "TestPackage")
    Set<CaseDetails> CaseDetails = new HashSet<>();*/

	public TestPackage(){}
	
	public TestPackage(Long packageId, String packageCd, String packageTests) {
		this.packageId = packageId;
		this.packageCd = packageCd;
		this.packageTests = packageTests;
	}

	public Long getPackageId() {
		return packageId;
	}

	public void setPackageId(Long packageId) {
		this.packageId = packageId;
	}

	public String getPackageCd() {
		return packageCd;
	}

	public void setPackageCd(String packageCd) {
		this.packageCd = packageCd;
	}

	public String getPackageTests() {
		return packageTests;
	}

	public void setPackageTests(String packageTests) {
		this.packageTests = packageTests;
	}

	/*public Set<CaseDetails> getPosts() {
		return CaseDetails;
	}

	public void setPosts(Set<CaseDetails> posts) {
		this.CaseDetails = posts;
	}
*/
	@Override
	public String toString() {
		return "Package [packageId=" + packageId + ", packageCd=" + packageCd + ", packageTests=" + packageTests + "]";
	}
	
}
