package com.application.iqfirst.pojo;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.JoinColumn;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "CaseDetails")
public class CaseDetails implements Serializable {
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "CaseId")
	Long 	caseId;
	
	@Column(name = "InCompId")
	Integer inCompId;
	
	@Column(name = "PolicyNo")
	String 	policyNo;
	
	@Column(name = "CustomerName")
	String 	customerName;
	
	@Column(name = "Address")
	String 	address;
	
	@Column(name = "Pin")
	String 	pin;
	
	@Column(name = "LatLng")
	String 	latLng;
	
	@Column(name = "RegDt")
	String 	regDt;
	
	@Column(name = "CustomerCO")
	String customerCO;
	
	@Column(name = "City")
	String city;
	
	@Column(name = "State")
	String state;
	
	@Column(name = "MobileNo1")
	String mobileNo1;
	
	@Column(name = "MobileNo2")
	String mobileNo2;
	
	@Column(name = "SA")
	String sa;
	
	/*@ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                CascadeType.PERSIST,
                CascadeType.MERGE
            })*/
	@ManyToMany(cascade=CascadeType.ALL)
	@JoinTable(name = "CasePkgMapping",
            joinColumns = { @JoinColumn(name = "CaseId") },
            inverseJoinColumns = { @JoinColumn(name = "PackageId") })
    Set<TestPackage> TestPackage = new HashSet<>();

	
	@Column(name = "CaseType")
	String caseType;
	
	@Column(name = "PaymentBy")
	String paymentBy;
	
	
	public CaseDetails() {}

	

	public CaseDetails(Integer inCompId, String policyNo, String customerName, String address, String pin,
			String latLng, String regDt, String customerCO, String city, String state, String mobileNo1,
			String mobileNo2, String sa, String caseType, String paymentBy) {
		this.inCompId = inCompId;
		this.policyNo = policyNo;
		this.customerName = customerName;
		this.address = address;
		this.pin = pin;
		this.latLng = latLng;
		this.regDt = regDt;
		this.customerCO = customerCO;
		this.city = city;
		this.state = state;
		this.mobileNo1 = mobileNo1;
		this.mobileNo2 = mobileNo2;
		this.sa = sa;
		this.caseType = caseType;
		this.paymentBy = paymentBy;
	}

	public Long getCaseId() {
		return caseId;
	}

	public void setCaseId(Long caseId) {
		this.caseId = caseId;
	}

	public Integer getInCompId() {
		return inCompId;
	}

	public void setInCompId(Integer inCompId) {
		this.inCompId = inCompId;
	}

	public String getPolicyNo() {
		return policyNo;
	}

	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public String getLatLng() {
		return latLng;
	}

	public void setLatLng(String latLng) {
		this.latLng = latLng;
	}

	public String getRegDt() {
		return regDt;
	}

	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}

	public String getCustomerCO() {
		return customerCO;
	}

	public void setCustomerCO(String customerCO) {
		this.customerCO = customerCO;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getMobileNo1() {
		return mobileNo1;
	}

	public void setMobileNo1(String mobileNo1) {
		this.mobileNo1 = mobileNo1;
	}

	public String getMobileNo2() {
		return mobileNo2;
	}

	public void setMobileNo2(String mobileNo2) {
		this.mobileNo2 = mobileNo2;
	}

	public String getSa() {
		return sa;
	}

	public void setSa(String sa) {
		this.sa = sa;
	}

	public String getCaseType() {
		return caseType;
	}

	public void setCaseType(String caseType) {
		this.caseType = caseType;
	}

	public String getPaymentBy() {
		return paymentBy;
	}

	public void setPaymentBy(String paymentBy) {
		this.paymentBy = paymentBy;
	}

	public Set<TestPackage> getTestPackage() {
		return TestPackage;
	}

	public void setTestPackage(Set<TestPackage> testPackage) {
		this.TestPackage = testPackage;
	}



	@Override
	public String toString() {
		return "CaseDetails [caseId=" + caseId + ", inCompId=" + inCompId + ", policyNo=" + policyNo + ", customerName="
				+ customerName + ", address=" + address + ", pin=" + pin + ", latLng=" + latLng + ", regDt=" + regDt
				+ ", customerCO=" + customerCO + ", city=" + city + ", state=" + state + ", mobileNo1=" + mobileNo1
				+ ", mobileNo2=" + mobileNo2 + ", sa=" + sa + ", caseType=" + caseType
				+ ", paymentBy=" + paymentBy + "]";
	}
	
}
