package com.application.iqfirst.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.application.iqfirst.pojo.CaseDetails;
import com.application.iqfirst.pojo.InsuranceCompany;
import com.application.iqfirst.pojo.TestPackage;
import com.application.iqfirst.repository.IQFirstInsuranceCompanyRepository;
import com.application.iqfirst.repository.IQFirstPackageRepository;
import com.application.iqfirst.service.impl.IQFirstPackageServiceImpl;

@Service
public class FileReader {
	@Autowired
	IQFirstPackageRepository iQFirstPackageRepository;
	
	@Autowired
	IQFirstInsuranceCompanyRepository iQFirstInsuranceCompanyRepository;
	
	public List<CaseDetails> readCasesFromCSV(String fileName) { 
		List<CaseDetails> listCaseDetails = new ArrayList<>(); 
		Path pathToFile = Paths.get(fileName); 
		try (BufferedReader br = Files.newBufferedReader(pathToFile, StandardCharsets.US_ASCII)) { 
				String line = br.readLine();  
				int lineCount = 0;
				while (line != null) { 
					String[] attributes = line.split(","); 
					lineCount ++;
					if(lineCount > 1){
						CaseDetails caseDetails = createCases(attributes); 
						listCaseDetails.add(caseDetails);
					}
					line = br.readLine();
				} 
			} catch (IOException ioe) { 
				ioe.printStackTrace(); 
			} 
		return listCaseDetails; 
	}
	
	private CaseDetails createCases(String[] metadata) { 
		CaseDetails caseDetails = new CaseDetails();
		Integer inCompId 	= Integer.parseInt(metadata[0]);
		String 	policyNo 	= metadata[1].toString();
		String 	caseName 	= metadata[2].toString();
		String 	address 	= metadata[3].toString();
		String 	pin 		= metadata[4].toString();
		String 	latLng 		= metadata[5].toString();
		String 	regDt 		= metadata[6].toString(); 
		//return new CaseDetails(inCompId, policyNo, caseName, address, pin, latLng, regDt);
		return caseDetails;
	}
	
	public List<CaseDetails> readCasesFromXLSX(String fileName){
		CaseDetails caseDetails = null;
		List<CaseDetails> listCaseDetails = new ArrayList<>();
		try {

            FileInputStream excelFile = new FileInputStream(new File(fileName));
            Workbook workbook = new XSSFWorkbook(excelFile);
            Sheet datatypeSheet = workbook.getSheetAt(0);
            Iterator<Row> iterator = datatypeSheet.iterator();

            while (iterator.hasNext()) {
                Row currentRow = iterator.next();
                if(currentRow.getRowNum() != 0){
                	/*caseDetails = new CaseDetails(Integer.parseInt(String.format("%.0f",currentRow.getCell(0).getNumericCellValue())),String.format("%.0f",currentRow.getCell(1).getNumericCellValue()),
                			currentRow.getCell(2).toString(),currentRow.getCell(3).toString(),String.format("%.0f",currentRow.getCell(4).getNumericCellValue()),
                			currentRow.getCell(5).toString(),currentRow.getCell(6).toString());*/
                	caseDetails = createCaseDetails(currentRow);
                	listCaseDetails.add(caseDetails);
               }
            }
            workbook.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
		return listCaseDetails; 
	}
	
	public List<CaseDetails> readCasesFromXLS(String fileName){
		CaseDetails caseDetails = null;
		List<CaseDetails> listCaseDetails = new ArrayList<>();
		try{
			FileInputStream excelFile 		= new FileInputStream(new File(fileName));
			HSSFWorkbook 	hSSFWorkbook 	= new HSSFWorkbook(excelFile); 
			HSSFSheet 		sheet			= hSSFWorkbook.getSheetAt(0); 
			Iterator<Row> iterator = sheet.iterator();
			 while (iterator.hasNext()) {
	                Row currentRow = iterator.next();
	                if(currentRow.getRowNum() != 0){
	                	/*caseDetails = new CaseDetails(Integer.parseInt(String.format("%.0f",currentRow.getCell(0).getNumericCellValue())),String.format("%.0f",currentRow.getCell(1).getNumericCellValue()),
	                			currentRow.getCell(2).toString(),currentRow.getCell(3).toString(),String.format("%.0f",currentRow.getCell(4).getNumericCellValue()),
	                			currentRow.getCell(5).toString(),currentRow.getCell(6).toString());*/
	                	caseDetails = createCaseDetails(currentRow);
	                	listCaseDetails.add(caseDetails);
	               }
	            }
			 hSSFWorkbook.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return listCaseDetails; 
	}
	
	public CaseDetails createCaseDetails(Row o){
		//List<PackageDe>
		CaseDetails caseDetails = new CaseDetails();
		//System.out.println(o.getClass().getName().);
		String incName = o.getCell(0).toString();
		String policyKey = String.format("%.0f",o.getCell(1).getNumericCellValue());
		String customerName = o.getCell(2).toString();
		String address = o.getCell(3).toString();
		String pinCode = String.format("%.0f",o.getCell(4).getNumericCellValue());
		String latlang = o.getCell(5).toString();
		String regDate = o.getCell(6).toString();
		String customerCO = o.getCell(7).toString();
		String city = o.getCell(8).toString();
		String state = o.getCell(9).toString();
		String mob1 = String.format("%.0f",o.getCell(10).getNumericCellValue());
		String mob2 = String.format("%.0f",o.getCell(11).getNumericCellValue());
		String sa = o.getCell(12).toString();
		String testName = o.getCell(13).toString();
		String caseType = o.getCell(14).toString();
		String paymentBy = o.getCell(15).toString();
		
		long incId = getInCompId(incName);
		
		caseDetails.setInCompId((int)incId);
		caseDetails.setPolicyNo(policyKey);
		caseDetails.setCustomerName(customerName);
		caseDetails.setAddress(address);
		caseDetails.setPin(pinCode);
		caseDetails.setLatLng(latlang);
		caseDetails.setRegDt(regDate);
		caseDetails.setCustomerCO(customerCO);
		caseDetails.setCity(city);
		caseDetails.setState(state);
		caseDetails.setMobileNo1(mob1);
		caseDetails.setMobileNo2(mob2);
		caseDetails.setSa(sa);
		caseDetails.setPaymentBy(paymentBy);
		caseDetails.setCaseType(caseType);
		
		Set<TestPackage> packageIds = getPackageIds(testName);
		
		for(TestPackage packageDetails : packageIds){
			//caseDetails.getTestPackage().add(packageDetails);
			caseDetails.setTestPackage(packageIds);
			//packageDetails.getPosts().add(caseDetails);
		}
		
		return caseDetails;
		
		//return new CaseDetails((int)incId, policyKey, customerName, address, pinCode, latlang, regDate, customerCO, city, state, mob1, mob2, sa, caseType, paymentBy);
		
	}
	
	public Set<TestPackage> getPackageIds(String testnames){
		Set<TestPackage> pkgdtls = new HashSet<TestPackage>();
		TestPackage packageDetails = null;
		//StringBuffer packageIds = new StringBuffer();
		System.out.println("testpacakges:"+testnames);
		String[] testPackages = testnames.split(",");
		for(String tests: testPackages){
			packageDetails = iQFirstPackageRepository.findByName(tests);
			/*packageIds.append(packageDetails.getPackageId());//Long.toString(packageDetails.getPackageId());
			packageIds.append(",");*/
			pkgdtls.add(packageDetails);
		}
		return pkgdtls;
	}
	
	public Long getInCompId(String name){
		InsuranceCompany insuranceCompany = iQFirstInsuranceCompanyRepository.findByName(name);
		return insuranceCompany.getCompId();
	}
	
	public String getFileExtension(String file) {
        if(file.lastIndexOf(".") != -1 && file.lastIndexOf(".") != 0)
        return file.substring(file.lastIndexOf(".")+1);
        else return "";
    }
	
	public static void main(String[] args){
		FileReader fileReader = new FileReader();
		IQFirstPackageServiceImpl IQFirstPackageServiceImpl = new IQFirstPackageServiceImpl();
		/*System.out.println("Hello program staretd==========");
		String extension = fileReader.getFileExtension("E:\\CasesList.csv");
		System.out.println(extension);*/
		/*List<CaseDetails> allCases = fileReader.readCasesFromCSV("E:\\CasesList.csv");
		for (CaseDetails caseDetails : allCases){
			System.out.println("CaseDetails: "+caseDetails);
		}*/
		List<CaseDetails> allCases = fileReader.readCasesFromXLSX("E:\\LatestCaseList.xlsx");
		for (CaseDetails caseDetails : allCases){
			System.out.println("CaseDetails: "+caseDetails);
		}
	}
}
