package com.application.iqfirst.repository;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.application.iqfirst.pojo.InsuranceCompany;
import com.application.iqfirst.service.IQFirstIncCompService;

@Repository
public interface IQFirstInsuranceCompanyRepository extends CrudRepository<InsuranceCompany, Long>, IQFirstIncCompService {
	

}
