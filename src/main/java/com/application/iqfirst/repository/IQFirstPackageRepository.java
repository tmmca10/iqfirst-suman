package com.application.iqfirst.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.application.iqfirst.pojo.TestPackage;
import com.application.iqfirst.service.IQFirstPackageService;

@Repository
public interface IQFirstPackageRepository extends CrudRepository<TestPackage, Long>, IQFirstPackageService {

}
