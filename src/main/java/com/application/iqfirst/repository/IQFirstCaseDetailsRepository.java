package com.application.iqfirst.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.application.iqfirst.pojo.CaseDetails;

@Repository
public interface IQFirstCaseDetailsRepository extends CrudRepository<CaseDetails, Long> {

}
