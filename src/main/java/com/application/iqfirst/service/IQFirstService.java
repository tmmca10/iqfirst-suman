package com.application.iqfirst.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.application.iqfirst.pojo.CaseDetails;
import com.application.iqfirst.pojo.InsuranceCompany;
import com.application.iqfirst.pojo.TestPackage;

@Service
public interface IQFirstService {
	public List<CaseDetails> listAll();
        
    public void save(CaseDetails newCase);
     
    public CaseDetails get(long id);
     
    public void delete(long id);
    
    public List<CaseDetails> readCaseDetailsFromFile(String fileName);
    
    public InsuranceCompany findIncCompIDByName(String icName);
    
    public TestPackage findPackageDetailsIdByName(String name);

}
