package com.application.iqfirst.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.application.iqfirst.pojo.InsuranceCompany;

@Service
public interface IQFirstIncCompService {
	InsuranceCompany findByName(String name);

}
