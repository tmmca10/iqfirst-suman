package com.application.iqfirst.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.application.iqfirst.pojo.CaseDetails;
import com.application.iqfirst.pojo.InsuranceCompany;
import com.application.iqfirst.pojo.TestPackage;
import com.application.iqfirst.repository.IQFirstCaseDetailsRepository;
import com.application.iqfirst.repository.IQFirstInsuranceCompanyRepository;
import com.application.iqfirst.repository.IQFirstPackageRepository;
import com.application.iqfirst.service.IQFirstService;
import com.application.iqfirst.util.FileReader;

@Service
@Transactional
public class IQFirstServiceImpl implements IQFirstService {

	@Autowired
	private IQFirstCaseDetailsRepository iQFirstRepository;
	
	@Autowired
	private IQFirstInsuranceCompanyRepository iQFirstInsuranceCompanyRepository;
	
	@Autowired
	private IQFirstPackageRepository iQFirstPackageRepository;
	
	@Autowired
	private FileReader fileReader;
	
	@Override
	public List<CaseDetails> listAll() {
		//return iQFirstRepository.findAll();
		return null;
	}

	@Override
	public void save(CaseDetails newCase) {
		iQFirstRepository.save(newCase);
	}

	@Override
	public CaseDetails get(long id) {
		return iQFirstRepository.findById(id).get();
	}

	@Override
	public void delete(long id) {
		iQFirstRepository.deleteById(id);
	}

	@Override
	public List<CaseDetails> readCaseDetailsFromFile(String fileName) {
		List<CaseDetails> allCases  = new ArrayList<>();
		String 	extension = fileReader.getFileExtension(fileName);
		
		if(extension.equalsIgnoreCase("csv")){
			allCases = fileReader.readCasesFromCSV(fileName);
		}else if(extension.equalsIgnoreCase("xlsx")){
			allCases = fileReader.readCasesFromXLSX(fileName);
		}else if(extension.equalsIgnoreCase("xls")){
			allCases = fileReader.readCasesFromXLS(fileName);
		}
		return allCases;
	}

	@Override
	public InsuranceCompany findIncCompIDByName(String icName) {
		return iQFirstInsuranceCompanyRepository.findByName(icName.trim());
	}

	@Override
	public TestPackage findPackageDetailsIdByName(String name) {
		return iQFirstPackageRepository.findByName(name);
	}

}
