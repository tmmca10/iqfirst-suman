package com.application.iqfirst.service.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.application.iqfirst.pojo.TestPackage;
import com.application.iqfirst.service.IQFirstPackageService;

public class IQFirstPackageServiceImpl implements IQFirstPackageService {

	@Override
	public TestPackage findByName(String packageName) {

		List<TestPackage> packages = new ArrayList<TestPackage>();
		TestPackage packageDetails = null;

		 String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";  
		 String DB_URL = "jdbc:mysql://104.45.151.68:3306/iqfirsthealth";

		   //  Database credentials
		   String USER = "iqfirst";
		   String PASS = "IQFirst@2020";
		   
		   
		   Connection conn = null;
		   PreparedStatement stmt = null;
		   try{
			   Class.forName(JDBC_DRIVER);
			   conn = DriverManager.getConnection(DB_URL,USER,PASS);
			   String sql = "SELECT PackageId, PackageCd, PackageTests FROM TestPackage WHERE PackageCd='"+packageName.trim()+"'";
			   stmt = conn.prepareStatement(sql);
			   ResultSet rs = stmt.executeQuery();
			   while(rs.next()){
				   packageDetails = new TestPackage(rs.getLong("PackageId"), rs.getString("PackageCd"),rs.getString("PackageTests"));
			   }
			   rs.close();
			   stmt.close();
			   conn.close();
		   }catch(SQLException se){
			      //Handle errors for JDBC
			      se.printStackTrace();
		   }catch(Exception e){
			      //Handle errors for Class.forName
			      e.printStackTrace();
		   }

		return packageDetails;
	
	}
	
	public void dataRefator(){
		String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";  
		 String DB_URL = "jdbc:mysql://104.45.151.68:3306/iqfirsthealth";

		   //  Database credentials
		   String USER = "iqfirst";
		   String PASS = "IQFirst@2020";
		   
		   
		   Connection conn = null;
		   PreparedStatement stmt = null;
		   try{
			   Class.forName(JDBC_DRIVER);
			   conn = DriverManager.getConnection(DB_URL,USER,PASS);
			   String sql = "DELETE t1 FROM TestPackage t1 INNER JOIN TestPackage t2 WHERE t1.seqno < t2.seqno AND t1.PackageCd = t2.PackageCd;";
			   stmt = conn.prepareStatement(sql);
			   stmt.executeQuery();
			   //ResultSet rs = stmt.executeQuery();
			   //rs.close();
			   stmt.close();
			   conn.close();
		   }catch(SQLException se){
			      //Handle errors for JDBC
			      se.printStackTrace();
		   }catch(Exception e){
			      //Handle errors for Class.forName
			      e.printStackTrace();
		   }

	}

}
