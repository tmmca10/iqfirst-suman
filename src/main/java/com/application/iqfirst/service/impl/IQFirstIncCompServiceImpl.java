package com.application.iqfirst.service.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.application.iqfirst.pojo.InsuranceCompany;
import com.application.iqfirst.service.IQFirstIncCompService;

public class IQFirstIncCompServiceImpl implements IQFirstIncCompService {

	@Override
	public InsuranceCompany findByName(String name) {
		InsuranceCompany insuranceCompany = null;

		 String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";  
		 String DB_URL = "jdbc:mysql://104.45.151.68:3306/iqfirsthealth";

		   //  Database credentials
		   String USER = "iqfirst";
		   String PASS = "IQFirst@2020";
		   
		   
		   Connection conn = null;
		   PreparedStatement stmt = null;
		   try{
			   Class.forName(JDBC_DRIVER);
			   conn = DriverManager.getConnection(DB_URL,USER,PASS);
			   String sql = "SELECT CompId, CompName FROM InsuranceCompany WHERE CompName='"+name.trim()+"'";
			   stmt = conn.prepareStatement(sql);
			   ResultSet rs = stmt.executeQuery();
			   while(rs.next()){
				   insuranceCompany = new InsuranceCompany(rs.getLong("CompId"), rs.getString("CompName"));
			   }
			   rs.close();
			   stmt.close();
			   conn.close();
		   }catch(SQLException se){
			      //Handle errors for JDBC
			      se.printStackTrace();
		   }catch(Exception e){
			      //Handle errors for Class.forName
			      e.printStackTrace();
		   }

		return insuranceCompany;
	}

}
