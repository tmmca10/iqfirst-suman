package com.application.iqfirst.service;

import java.util.List;

import com.application.iqfirst.pojo.TestPackage;

public interface IQFirstPackageService {
	TestPackage findByName(String packageName);
	
	public void dataRefator();

}
