package com.application.iqfirst.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.application.iqfirst.pojo.CaseDetails;
import com.application.iqfirst.pojo.InsuranceCompany;
import com.application.iqfirst.pojo.TestPackage;
import com.application.iqfirst.repository.IQFirstPackageRepository;
import com.application.iqfirst.service.IQFirstService;

@RestController
@RequestMapping("/")
public class IQFirstRestController {
	
	@Autowired
	IQFirstService iQFirstService;
	
	@Autowired
	IQFirstPackageRepository iQFirstPackageRepository;
	
	@RequestMapping(value = "/save", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public String saveCases(@RequestBody CaseDetails newCase) {
		System.out.println("Printing the values from rest service: "+newCase);
		
		iQFirstService.save(newCase);
	    return "value saved successfully";
	}
	
	@RequestMapping(value = "/savefromfile", method = RequestMethod.POST)
	public String saveCaseFromFile() {
		List<CaseDetails> allCases = iQFirstService.readCaseDetailsFromFile("E:\\LatestCaseList.xlsx");
		for(CaseDetails caseDetails : allCases){
			iQFirstService.save(caseDetails);
			//System.out.println(caseDetails);
		}
		iQFirstPackageRepository.dataRefator();
	    return "values saved successfully";
	}
	
	/*@RequestMapping(value = "/getinccomp", method = RequestMethod.GET)
	public List<InsuranceCompany> getIncCompByName(@RequestParam(name = "name") String name){
		List<InsuranceCompany> inccomps = iQFirstService.findIncCompIDByName(name);
		for(InsuranceCompany InsuranceCompany : inccomps){
			System.out.println(InsuranceCompany);
		}
		return inccomps;
	}*/
	
	/*@RequestMapping(value = "/getpackage", method = RequestMethod.GET)
	public List<PackageDetails> getPackageByName(@RequestParam(name = "name") String name){
		List<PackageDetails> inccomps = iQFirstService.findPackageDetailsIdByName(name);
		for(PackageDetails PackageDetails : inccomps){
			System.out.println(PackageDetails);
		}
		return inccomps;
	}*/
	

}
